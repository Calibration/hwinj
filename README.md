# LIGO hardware injections

This project holds the script used to handle hardware injections at
the LIGO Observatories.

A YAML config is used to configure different injections for different
groups (e.g. "detchar", "stochastic", etc.).  The
`hardware_injections.py` reads the config file and executes the
specified injection group/name.  See the script --help for usage
description.

These injections should happen in the "nominal low noise" `ISC_LOCK`
guardian state.  Assuming everything is configured correctly and the
`DIAG_EXEC` guardian nodes are properly tied into the overall guardian
IFO status, initiation of these nodes should take the detector out of
OBSERVING mode automatically.


## DetChar injections


* [DetChar O4 hardware injection proposal](https://dcc.ligo.org/LIGO-G2101915)
* [DetChar waveform files](https://ldas-jobs.ligo.caltech.edu/~siddharth.soni/HW_Inj_Detchar_Waveforms/)
* [example gracedb upload](https://gracedb-test.ligo.org/events/T393840/view/)


## Stochastic

* [Stochastic hardware injection in O4](https://dcc.ligo.org/LIGO-T2100448)

#!/usr/bin/env python3

import os
import csv
import json
import random
import logging
import argparse
import subprocess

import yaml
import numpy

import awg
from ezca import Ezca
import gpstime
from ligo.gracedb.rest import GraceDb

##################################################

logging.basicConfig(
    level=os.getenv('LOG_LEVEL', 'INFO').upper(),
    # format='%(asctime)s %(message)s',
    # datefmt='%m%d%YT%H:%M:%S',
    format='%(message)s',
)
logger = logging.getLogger(__name__)


IFO = os.getenv('IFO')
if not IFO:
    raise RuntimeError("IFO env var not specified.")

CAL_ROOT = os.getenv(
    'CAL_ROOT',
    f"/ligo/groups/cal/{IFO}",
)
WAVEFORM_ROOT = os.getenv(
    'WAVEFORM_ROOT',
    f'{CAL_ROOT}/hwinj',
)
CONFIG_PATH = os.path.join(WAVEFORM_ROOT, 'hwinj.yaml')

GRACEDB_URL = 'https://gracedb.ligo.org/api/'
GRACEDB_GROUP = 'Detchar'
GRACEDB_PIPELINE = 'HardwareInjection'

EXC_CHANNEL = f"{IFO}:CAL-INJ_TRANSIENT_EXC"
TINJ_TYPE_MAP = {
    'cbc': 1,
    'burst': 2,
    'detchar': 3,
    'stochastic': 4,
}

##################################################

def ligo_proxy_init():
    """Initialize LIGO cert for GraceDB authentication

    """
    while True:
        try:
            subprocess.run(
                # FIXME: should we be using ecp-get-cert instead?
                #['ecp-get-cert', '--kerberos'],
                ['ligo-proxy-init', '--kerberos'],
                check=True,
                stderr=subprocess.PIPE,
            )
            break
        except subprocess.CalledProcessError:
            subprocess.run(
                ['kinit'],
                check=False,
            )


def parse_detchar_injection_log(log_file):
    """parse injection log CSV and generate injection dicts

    This function encodes the relative timing of the injections in the
    overall waveform.

    """
    # first waveform time is 10 seconds in
    initial_offset = 10

    # separation betwen waveforms in seconds
    delta_t = 5

    with open(log_file, encoding='utf-8') as csvf:
        csvReader = csv.DictReader(csvf)

        for ind, inj in enumerate(csvReader):
            inj['instruments'] = IFO
            inj['offset_seconds'] = initial_offset + delta_t*ind
            yield inj

##################################################

parser = argparse.ArgumentParser(
    description="""LIGO hardware injections

This script runs hardware injections specified from a hardware
injection YAML config file.  Different groups can be configured with
multiple different injection options.  The config should point to
waveform files stored locally.

A specific injection time can be specified, or if not specified the
injection will happen 10 seconds from execution time.  For the special
case of a "detchar" "safety" injection with an unspecified time the
injection time will be adjusted to not happen close to the second
boundary.  The script will block until the injection is complete.

The injection will not actually be run unless the --run option is
provided.  Use the --dry option to validate that the waveform file can
be read, or with no option just print out information about the
injection that would be run.""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
)
parser.add_argument(
    'group', nargs='?',
    help="injection group, e.g. 'detchar', 'stochastic'")
parser.add_argument(
    'name', nargs='?',
    help="injection name")
parser.add_argument(
    '--config', '-c', default=CONFIG_PATH,
    help=f"config file (default: {CONFIG_PATH})")
parser.add_argument(
    '--time', action=gpstime.GPSTimeParseAction,
    help="injection time (as GPS or relative or absolute time string, default: now+10s)")
parser.add_argument(
    '--gracedb-url', type=str,
    default=GRACEDB_URL,
    help=f"GraceDB url (default: {GRACEDB_URL})")
parser.add_argument(
    '--gracedb-group', type=str,
    default=GRACEDB_GROUP,
    help=f"GraceDB group (default: {GRACEDB_GROUP})")
rgroup = parser.add_mutually_exclusive_group()
rgroup.add_argument(
    '--dry', action='store_const', dest='run', const='dry',
    help="dry run injections")
rgroup.add_argument(
    '--run', action='store_const', dest='run', const='run',
    help="actualy run injections")


def main():
    args = parser.parse_args()
    logger.debug(args)

    logger.info(f"ifo: {IFO}")
    logger.info(f"waveform root: {WAVEFORM_ROOT}")
    logger.info(f"config file: {args.config}")

    with open(args.config) as f:
        config = yaml.safe_load(f)

    gracedb_url = args.gracedb_url
    gracedb_group = args.gracedb_group

    logger.info(f"GraceDB url: {gracedb_url}")
    logger.info(f"GraceDB group: {gracedb_group}")
    logger.info(f"GraceDB pipeline: {GRACEDB_PIPELINE}")
    logger.info(f"excitation channel: {EXC_CHANNEL}")

    if not args.group or not args.name:
        logger.error("must specify injection group and name, options are (group: name(s)):")
        for group, name_dict in config.items():
            names = ', '.join(list(name_dict))
            logger.error(f"  {group}: {names}")
        raise SystemExit(1)

    try:
        cgroup = config[args.group]
    except Exception:
        parser.error(f'unknown injection group "{args.group}"')

    try:
        injection = cgroup[args.name]
    except Exception:
        parser.error(f'unknown injection name "{args.name}"')

    logger.info(f"injection group: {args.group}")
    logger.info(f"injection name: {args.name}")

    injection['waveform'] = os.path.join(WAVEFORM_ROOT, injection['waveform'])
    logger.info("reading waveform file...")
    waveform = numpy.loadtxt(injection['waveform'])
    inj_length = len(waveform) / injection['rate']
    logger.info(f"injection waveform file: {injection['waveform']}")
    logger.info(f"injection waveform sample rate: {injection['rate']}")
    logger.info(f"injection waveform length: {inj_length} seconds")

    injection['log'] = injection.get('log')
    if injection['log']:
        injection['log'] = os.path.join(WAVEFORM_ROOT, injection['log'])
        logger.info("reading log file...")
        detchar_injection_log = list(parse_detchar_injection_log(injection['log']))
        nlogs = len(detchar_injection_log)
        logger.info(f"injection log file: {injection['log']}")
        logger.info(f"injection log entries: {nlogs}")

    if not args.run:
        return

    if injection['log']:
        logger.info("initiating ligo_proxy_init for GraceDB access...")
        ligo_proxy_init()

    gps_now = gpstime.parse('now').gps()

    if args.time:
        gps_start = args.time.gps()
        if gps_start <= gps_now:
            raise parser.error(f"specified time is in the past: {gps_start} <= {gps_now}")
    else:
        gps_start = gps_now + 10
        # for safety injections we adjust the time to not be near the
        # second boundary
        if args.group == 'detchar' and args.name == 'safety':
            gps_start = int(gps_start) + random.uniform(0.15, 0.85)

    gps_start = round(gps_start, 2)

    logger.info(f"injection start GPS: {gps_start}")

    ########################################

    if args.run == 'run':

        ezca = Ezca()

        # transient injection filter module control object
        trans_filter = ezca.get_LIGOFilter(':CAL-INJ_TRANSIENT')

        # set the injection type
        ezca.write(':CAL-INJ_TINJ_TYPE', TINJ_TYPE_MAP[args.group])

        # turn the output on
        trans_filter.turn_on('OUTPUT')

        # create the AWG stream
        logger.info("=== EXECUTING AWG INJECTION ===")
        logger.info("this will wait until the injection is nearly complete...")
        stream = awg.ArbitraryStream(
            EXC_CHANNEL,
            rate=injection['rate'],
            start=gps_start,
        )
        stream.send(waveform)
        # FIXME: we have no way to handle ctrl-C signals

        # always turn the output back off
        trans_filter.turn_off('OUTPUT')

        # reset the injection type
        ezca.write(':CAL-INJ_TINJ_TYPE', 0)

        # FIXME: the send method above should block until the
        # injection is mostly complete.  but we should still figure
        # out how to check the stream explicitly.

        logger.info("=== INJECTION COMPLETE ===")

    ########################################

    if injection['log']:

        logger.info("creating injection events in GraceDB...")

        client = GraceDb(gracedb_url)

        # GraceDB upload
        for inj in detchar_injection_log:
            inj_time = gps_start + inj['offset_seconds']
            inj['gpstime'] = inj_time
            filename = injection['gracedb_filename_fmt'].format(IFO=IFO, inj_time=inj_time)
            inj_json = json.dumps(inj, indent=4)
            logger.info(f"creating event: {filename}")
            if args.run == 'run':
                client.create_event(
                    group=gracedb_group,
                    pipeline=GRACEDB_PIPELINE,
                    filename=filename,
                    filecontents=inj_json,
                )
            else:
                print(inj_json)
                print()

        if args.run == 'run':
            logger.info("uploads complete.")


if __name__ == '__main__':
    main()
